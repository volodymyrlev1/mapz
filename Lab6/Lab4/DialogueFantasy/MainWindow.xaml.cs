﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DialogueFantasy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly GameFacade _gameFacade;
       
        public MainWindow()
        {
            InitializeComponent();
            _gameFacade = new GameFacade(this);
            _gameFacade._gameManager.LevelsFinished += OnLevelsFinished;
            this.KeyDown += Window_KeyDown;
            this.KeyUp += Window_KeyUp;

            _gameFacade.StartGame();
        }

        private HashSet<Key> keysPressed = new HashSet<Key>();

        public TextBlock eventTextBlock { get; private set; }
        public TextBlock bonusTextBlock { get; private set; }
        public Button[] choiceButtons { get; private set; }
        public StackPanel questPanel { get; private set; }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            keysPressed.Add(e.Key);
            _gameFacade.MovePlayer(keysPressed.ToArray());
            _gameFacade.CheckPlayerNearby(e);
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            keysPressed.Remove(e.Key);
            Player.GetInstance().playerImage.Source = Player.GetInstance().directions[0];
        }

        public void InitializeUI()
        {
            eventTextBlock = new TextBlock();
            bonusTextBlock = new TextBlock();
            choiceButtons = new Button[4];

            questPanel = new StackPanel();
            questPanel.Width = 500;
            questPanel.Height = 300;
            questPanel.Background = Brushes.White;

            questPanel.Loaded += (sender, e) =>
            {
                double left = (GameArea.ActualWidth - questPanel.ActualWidth) / 2;
                double top = (GameArea.ActualHeight - questPanel.ActualHeight) / 2;
                Canvas.SetLeft(questPanel, left);
                Canvas.SetTop(questPanel, top);
            };

            bonusTextBlock.Foreground = Brushes.Magenta;

            bonusTextBlock.HorizontalAlignment = HorizontalAlignment.Center;
            eventTextBlock.HorizontalAlignment = HorizontalAlignment.Center;

            questPanel.Children.Add(bonusTextBlock);
            questPanel.Children.Add(eventTextBlock);

            for (int i = 0; i < 4; i++)
            {
                choiceButtons[i] = new Button();
                choiceButtons[i].Content = "Button " + (i + 1);
                choiceButtons[i].Margin = new Thickness(0, 10, 0, 10);
                choiceButtons[i].Click += ChoiceButton_Click;
                choiceButtons[i].Width = 0.6 * questPanel.Width;
                questPanel.Children.Add(choiceButtons[i]);
            }

            GameArea.Children.Add(questPanel);
        }

        public void OnNewLevel(object sender, NewLevelEventArgs e)
        {
            DisplayEvent(e.Event);
            DisplayChoices(e.Choices);
            DisplayBonus(e.Bonus);

            //Random random = new Random();
            //int randomNumber = random.Next(1, 21);
            //Image backgroundImage = new Image();

            //string path = $"res/{randomNumber}.jpg";

            //backgroundImage.Source = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute));
            //backgroundImage.Stretch = Stretch.Fill;

            //GameArea.Background = new ImageBrush()
            //{
            //    ImageSource = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute)),
            //    Stretch = Stretch.Fill
            //};
        }

        private void DisplayEvent(string eventText)
        {
            eventTextBlock.Text = eventText;
        }

        private void DisplayChoices(Dictionary<string,int> choices)
        {
            for (int i = 0; i < 4; i++)
            {
                if (i < choices.Count)
                {
                    choiceButtons[i].Content = choices.Keys.ElementAt(i);
                    choiceButtons[i].IsEnabled = true;
                }
                else
                {
                    choiceButtons[i].IsEnabled = false;
                }
            }
        }

        private void DisplayBonus(string bonus)
        {
            bonusTextBlock.Text = bonus;
        }

        private void ChoiceButton_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = (Button)sender;
            int choiceIndex = Array.IndexOf(choiceButtons, clickedButton);
            _gameFacade._gameManager.HandleChoice(eventTextBlock.Text, clickedButton.Content.ToString());
        }

        public void OnHealthChanged(object sender, EventArgs e)
        {
            UpdateHealth();
        }
        private void UpdateHealth()
        {
            HealthBar.Children.Clear();

            for (int i = 0; i < _gameFacade._player.Health; i++)
            {
                Image heart = new Image();
                heart.Source = new BitmapImage(new Uri(@"pack://application:,,,/DialogueFantasy;component/res/heart.png", UriKind.Absolute));
                heart.Width = 100;
                heart.Height = 100;
                HealthBar.Children.Add(heart);
            }
        }

        private void OnLevelsFinished(object sender, EventArgs e)
        {
            CloseQuest();
        }
        private void CloseQuest()
        {
            questPanel.Visibility = Visibility.Collapsed;
        }
    }
}
