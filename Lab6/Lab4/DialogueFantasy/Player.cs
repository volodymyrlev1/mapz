﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DialogueFantasy
{
    public class Player
    {
        public DeathHandler deathHandler = new DeathHandlerNoBlessing();

        private static Player instance;
        private int _health;

        public Image playerImage { get; set; }
        public double xPosition { get; set; }
        public double yPosition { get; set; }

        public int stepSize { get; set; } = 20;
        public bool IsBlessed { get; set; } = false;

        public int Health
        {
            get { return _health; }
            set
            {
                _health = value;
                OnHealthChanged(EventArgs.Empty);

                if (_health == 0)
                {
                    deathHandler.HandleDeath(this);
                }
            }
        }

        public List<BitmapImage> directions = new List<BitmapImage>(3);

        private Player() 
        {
            deathHandler = new DeathHandlerNoBlessing();
        }

        public static Player GetInstance()
        {
            if (instance == null)
            {
                instance = new Player();
            }
            return instance;
        }



        public event EventHandler HealthChanged;
        public void OnHealthChanged(EventArgs e)
        {
            HealthChanged?.Invoke(this, e);
        }

        public void UpdatePlayerPosition()
        {
            Canvas.SetLeft(playerImage, xPosition);
            Canvas.SetTop(playerImage, yPosition);
        }
    }

    public class PlayerController
    {
        private Player _player;
        private PlayerView _playerView;
        private int stepSize = 20;

        public PlayerController(Player player, Canvas canvas)
        {
            _player = player;
            _playerView = new PlayerView(player);
            Initialize(canvas);
        }

        public void Initialize(Canvas canvas)
        {
            _player.playerImage = new Image();

            _player.directions.Add(new BitmapImage(new Uri("res/character.png", UriKind.Relative)));
            _player.directions.Add(new BitmapImage(new Uri("res/left.png", UriKind.Relative)));
            _player.directions.Add(new BitmapImage(new Uri("res/right.png", UriKind.Relative)));

            _player.playerImage.Source = _player.directions[0];

            _player.playerImage.Width = 200;
            _player.playerImage.Height = 200;

            Canvas.SetLeft(_player.playerImage, 0);
            Canvas.SetTop(_player.playerImage, 0);

            canvas.Children.Add(_player.playerImage);
        }

        public void Move(Key[] keys, double canvasWidth, double canvasHeight)
        {
            double newX = _player.xPosition;
            double newY = _player.yPosition;

            bool moveHorizontal = false;
            bool moveVertical = false;

            foreach (var key in keys)
            {
                switch (key)
                {
                    case Key.W:
                        newY = Math.Max(0, _player.yPosition - _player.stepSize);
                        moveVertical = true;
                        _player.playerImage.Source = _player.directions[0];
                        break;
                    case Key.S:
                        newY = Math.Min(canvasHeight - _player.playerImage.Height, _player.yPosition + _player.stepSize);
                        moveVertical = true;
                        _player.playerImage.Source = _player.directions[0];
                        break;
                    case Key.A:
                        newX = Math.Max(0, _player.xPosition - _player.stepSize);
                        moveHorizontal = true;
                        _player.playerImage.Source = _player.directions[1];
                        break;
                    case Key.D:
                        newX = Math.Min(canvasWidth - _player.playerImage.Width, _player.xPosition + _player.stepSize);
                        moveHorizontal = true;
                        _player.playerImage.Source = _player.directions[2];
                        break;
                }
            }

            if (moveHorizontal && moveVertical)
            {
                double diagonalStep = _player.stepSize;
                _player.xPosition = newX;
                _player.yPosition = newY;
            }
            else if (moveHorizontal)
            {
                _player.xPosition = newX;
            }
            else if (moveVertical)
            {
                _player.yPosition = newY;
            }

            _playerView.UpdatePlayerPosition();
        }
    }

    public class PlayerView
    {
        private Player _player;

        public PlayerView(Player player)
        {
            _player = player;
        }

        public void UpdatePlayerPosition()
        {
            _player.UpdatePlayerPosition();
        }
    }

    public abstract class DeathHandler
    {
        public DeathHandler NextHandler;

        public void SetNextHandler(DeathHandler nextHandler)
        {
            this.NextHandler = nextHandler;
        }

        public abstract void HandleDeath(Player p);
    }

    public class DeathHandlerNoBlessing : DeathHandler
    {
        public override void HandleDeath(Player p)
        {
            if (!p.IsBlessed)
            {
                Application.Current.Shutdown();
            }

            NextHandler?.HandleDeath(p);
        }
    }

    public class DeathHandlerBlessed : DeathHandler
    {
        public override async void HandleDeath(Player p)
        {
            p.IsBlessed = false;
            p.Health = 3;
            p.deathHandler = new DeathHandlerNoBlessing();
            NextHandler?.HandleDeath(p);
        }
    }

    public class DeathHandlerChangeModel : DeathHandler
    {
        public override void HandleDeath(Player p)
        {
            BitmapImage img = new BitmapImage(new Uri("res/zombie.png", UriKind.Relative));
            p.playerImage.Source = img;
            
            for(int i = 0; i < p.directions.Count; i++)
            {
                p.directions[i] = img;
            }

            NextHandler?.HandleDeath(p);
        }
    }
}
