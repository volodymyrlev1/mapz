﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.TextFormatting;

namespace DialogueFantasy
{
    // Prototype pattern
    interface IClone
    {
        object Clone();
    }

    interface IRenderableDecorator
    {
        void Render(Canvas canvas);
        void SetImage(BitmapImage BI);
        void SetPosition(double newX, double newY);
        void SetDialogues(List<string> dialogues);
    }

    public interface ICharacterState
    {
        void Talk(Character character);
    }

    public class InitialState : ICharacterState
    {
        public async void Talk(Character character)
        {
            foreach (var r in character.dialogues)
            {
                character.dialogueTextBlock.Text = r;
                await Task.Delay(2000);
            }

            character.State = new TalkedState();
        }
    }

    class TalkedState : ICharacterState
    {
        public async void Talk(Character character)
        {
            character.dialogueTextBlock.Text = "Do you need something else?";
            await Task.Delay(2000);
            character.dialogueTextBlock.Text = "";
        }
    }

    public class Character : IClone
    {
        public Image characterImage { get; set; }
        public double xPosition { get; set; }
        public double yPosition { get; set; }
        public string name { get; set; }
        public int? Level { get; set; }

        public List<string> dialogues = new List<string>();
        public TextBlock dialogueTextBlock { get; set; }

        public ICharacterState State { get; set; }

        public Character(string characterName, string imagePath, double initialX, double initialY)
        {
            name = characterName;
            characterImage = new Image();
            characterImage.Source = new BitmapImage(new Uri(imagePath, UriKind.Relative));
            characterImage.Width = 200;
            characterImage.Height = 200;
            xPosition = initialX;
            yPosition = initialY;

            dialogueTextBlock = new TextBlock
            {
                FontSize = 24,
                Foreground = Brushes.Black,
                TextWrapping = TextWrapping.Wrap,
                Text = ""
            };

            SetPosition(initialX, initialY);
            Level = null;
            State = new InitialState();
        }

        public void SetPosition(double newX, double newY)
        {
            xPosition = newX;
            yPosition = newY;
            Canvas.SetLeft(characterImage, xPosition);
            Canvas.SetTop(characterImage, yPosition);
            Canvas.SetLeft(dialogueTextBlock, xPosition);
            Canvas.SetTop(dialogueTextBlock, yPosition + characterImage.Height);
        }

        public void AddToCanvas(Canvas canvas)
        {
            canvas.Children.Add(characterImage);
            canvas.Children.Add(dialogueTextBlock);
        }

        public async void Talk()
        {
            State.Talk(this);
        }

        public object Clone()
        {
            return new Character(name, characterImage.Source.ToString(), xPosition, yPosition)
            {
                dialogues = new List<string>(dialogues),
                Level = Level
            };
        }
    }

    public class CharacterDecorator : IRenderableDecorator
    {
        private Character character;

        public CharacterDecorator(Character character)
        {
            this.character = character;
        }

        public void Render(Canvas canvas)
        {
            character.AddToCanvas(canvas);
        }

        public void SetDialogues(List<string> dialogues)
        {
            character.dialogues = dialogues;
        }

        public void SetImage(BitmapImage BI)
        {
            character.characterImage.Source = BI;
        }

        public void SetPosition(double newX, double newY)
        {
            character.SetPosition(newX, newY);
        }
    }

    public class LocationDecorator : IRenderableDecorator
    {
        private Character character;

        public bool QuestAvailable { get; set; } = true;

        public LocationDecorator(Character c)
        {
            this.character = c;
        }

        public void Render(Canvas canvas)
        {
            character.AddToCanvas(canvas);
        }

        public void SetImage(BitmapImage BI)
        {
            character.characterImage.Source = BI;
        }

        public void SetPosition(double newX, double newY)
        {
            character.SetPosition(newX, newY);
        }

        public void SetDialogues(List<string> dialogues)
        {
            character.dialogues = dialogues;
        }
        public void SetSize(int width, int height)
        {
            character.characterImage.Width = width;
            character.characterImage.Height = height;
        }
    }
}
