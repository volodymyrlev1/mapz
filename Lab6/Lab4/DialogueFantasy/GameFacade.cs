﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DialogueFantasy
{
    public class GameFacade
    {
        private readonly PlayerController _playerController;
        private readonly MainWindow _mainWindow;

        private readonly Character _joe;
        private readonly Character _bob;
        private readonly Character _cave;

        public CharacterDecorator JoeDecorator { get; private set; }
        public CharacterDecorator BobDecorator {  get; private set; }
        public LocationDecorator CaveDecorator {  get; private set; }

        public GameManager _gameManager { get; }
        public Player _player { get; }

        public GameFacade(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            _gameManager = GameManager.Instance;

            _joe = new Character("Joe", "res/joe.png", 1600, 700);
            _bob = (Character)_joe.Clone();
            _cave = (Character)_joe.Clone();

            _bob.name = "Bob";
            _cave.name = "cave";
            _cave.Level = 1;

            JoeDecorator = new CharacterDecorator(_joe);
            BobDecorator = new CharacterDecorator(_bob);
            CaveDecorator = new LocationDecorator(_cave);

            _player = Player.GetInstance();
            _player.HealthChanged += mainWindow.OnHealthChanged;

            _playerController = new PlayerController(_player, _mainWindow.GameArea);


            InitializeCharacters();
        }

        private void InitializeCharacters()
        {
            JoeDecorator.SetDialogues(Dialogues.JoeDialogues);
            JoeDecorator.Render(_mainWindow.GameArea);
            
            BobDecorator.SetImage(new BitmapImage(new Uri("res/bob.png", UriKind.Relative)));
            BobDecorator.SetPosition(70, 650);
            BobDecorator.SetDialogues(Dialogues.BobDialogues);
            BobDecorator.Render(_mainWindow.GameArea);

            CaveDecorator.SetImage(new BitmapImage(new Uri("res/cave.png", UriKind.Relative)));
            CaveDecorator.SetSize(300, 300);
            CaveDecorator.SetPosition(1050, 50);
            CaveDecorator.SetDialogues(Dialogues.CaveDialogues);
            CaveDecorator.Render(_mainWindow.GameArea);
        }

        public void StartGame()
        {
            _player.Health = 3;
            _gameManager.StartGame(_mainWindow.GameArea);
        }

        public void MovePlayer(Key[] keys)
        {
            _playerController.Move(keys, _mainWindow.GameArea.ActualWidth, _mainWindow.GameArea.ActualHeight);
        }

        public void CheckPlayerNearby(KeyEventArgs e)
        {
            CheckPlayerNearCharacter(_joe, e);
            CheckPlayerNearCharacter(_bob, e);
            CheckPlayerNearQuest(_cave, e);
        }

        private void CheckPlayerNearCharacter(Character c, KeyEventArgs e)
        {
            double playerX = Canvas.GetLeft(_player.playerImage);
            double playerY = Canvas.GetTop(_player.playerImage);
            double cX = c.xPosition;
            double cY = c.yPosition;

            double proximityThreshold = 100;

            if (Math.Abs(playerX - cX) < proximityThreshold && Math.Abs(playerY - cY) < proximityThreshold && e.Key == Key.E)
            {
                c.Talk();
                if(c == _bob)
                {
                    _player.IsBlessed = true;
                    _player.deathHandler = new DeathHandlerBlessed();
                    _player.deathHandler.SetNextHandler(new DeathHandlerChangeModel());
                }
            }
            else
            {
                c.dialogueTextBlock.Text = "";
            }
        }

        private void CheckPlayerNearQuest(Character q, KeyEventArgs e)
        {
            double playerX = Canvas.GetLeft(_player.playerImage);
            double playerY = Canvas.GetTop(_player.playerImage);
            double qX = q.xPosition;
            double qY = q.yPosition;

            double proximityThreshold = 100;

            if (Math.Abs(playerX - qX) < proximityThreshold && Math.Abs(playerY - qY) < proximityThreshold && e.Key == Key.E)
            {
                q.Talk();
                _gameManager.NewLevel += _mainWindow.OnNewLevel;
                _mainWindow.InitializeUI();
                _gameManager.StartGame(_mainWindow.GameArea);
            }
            else
            {
                q.dialogueTextBlock.Text = "";
            }
        }
    }
}
