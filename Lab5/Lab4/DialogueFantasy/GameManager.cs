﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace DialogueFantasy
{
    public sealed class GameManager
    {
        public int GameCounter { get; set; } = 1;
        public StackPanel stackPanel;

        private Component _situation;

        private static GameManager instance;
        private static readonly object lockObj = new object();

        private static readonly Random random = new Random();
        private IEventBonusFactory eventBonusFactory;

        private int _levelCounter;
        private int LevelCounter
        {
            get => _levelCounter;
            set
            {
                if(_levelCounter == 10)
                {
                    _levelCounter = 1;

                    OnLevelsFinished(EventArgs.Empty);
                }

                _levelCounter++;
            }
        }

        private bool isNotInSituation = false;

        private IEvent currentEvent;

        public event EventHandler<NewLevelEventArgs> NewLevel;
        public event EventHandler LevelsFinished;

        public void OnLevelsFinished(EventArgs e)
        {
            LevelsFinished?.Invoke(this, e);
        }


        private GameManager()
        {
            _levelCounter = 1;
            SetRandomFactory();
            _situation = CaveSituation.root;
        }

        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObj)
                    {
                        if (instance == null)
                        {
                            instance = new GameManager();
                        }
                    }
                }
                return instance;
            }
        }

        private void SetRandomFactory()
        {
            int randomIndex = random.Next(2);
            eventBonusFactory = randomIndex == 0
                ? (IEventBonusFactory)new PeacefulEventPositiveBonusFactory()
                : new DangerousEventNegativeBonusFactory();
        }

        public void StartGame(Canvas canvas)
        {
            PlayLevel();
        }

        private void PlayLevel()
        {
            string eventText;
            Dictionary<string, int> choices;
            string bonus;

            if (isNotInSituation)
            {
                currentEvent = eventBonusFactory.CreateEvent();
                (eventText, choices) = currentEvent.GetEvent();
                bonus = string.Empty;

                if (LevelCounter % 3 == 0)
                {
                    bonus = eventBonusFactory.CreateBonus().GetBonus();
                }
            }
            else
            {
                currentEvent = null;

                eventText = _situation.Situation.Item1;
                choices = _situation.Situation.Item2;
                bonus = string.Empty;

                if (_situation is Leaf)
                {
                    isNotInSituation = true;
                }
            }


            OnNewLevel(new NewLevelEventArgs(eventText, choices, bonus));
            LevelCounter++;
            SetRandomFactory();
        }

        public void HandleChoice(int choiceIndex)
        {
            PlayLevel();
        }
        public void HandleChoice(string ev, string choice)
        {
            if (isNotInSituation && !(_situation is Leaf))
            {
                int value = -1;

                try
                {
                    value = GameDialogues.PeacefulEvents[ev][choice];
                }
                catch (Exception e)
                {
                    value = GameDialogues.DangerousEvents[ev][choice];
                }

                if (value == 0) Player.GetInstance().Health -= 1;

            }
            else
            {
                var value = _situation.Situation.Item2[choice];
                if (value == 0) Player.GetInstance().Health -= 1;

                if(!(_situation is Leaf))
                {
                    int i = 0;
                    foreach(var c in _situation.Situation.Item2)
                    {
                        if(c.Key == choice)
                        {
                            break;
                        }
                        i++;
                    }

                    _situation = ((Composite)_situation).children.ElementAt(i);
                }
                else
                {
                    _situation = null;
                }
            }
            PlayLevel();
        }

        public void HandleChoice(Dictionary<string,Dictionary<string,int>> choices, string choice) 
        {
            
            PlayLevel();
        }

        private void OnNewLevel(NewLevelEventArgs e)
        {
            NewLevel?.Invoke(this, e);
        }
    }

    public class NewLevelEventArgs : EventArgs
    {
        public string Event { get; }
        public Dictionary<string, int> Choices { get; }
        public string Bonus { get; }

        public NewLevelEventArgs(string eventText, Dictionary<string, int> choices, string bonus)
        {
            Event = eventText;
            Choices = choices;
            Bonus = bonus;
        }
    }
}
