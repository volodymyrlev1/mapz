﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogueFantasy
{
    public abstract class Component
    {
        public Tuple<string, Dictionary<string, int>> Situation { get; set; }

        public Component(string situation, Dictionary<string, int> options)
        {
            Situation = Tuple.Create(situation, options);
        }

        public abstract void Add(Component c);
        public abstract void Remove(Component c);
    }

    public class Composite : Component
    {
        public List<Component> children = new List<Component>();

        public Composite(string situation, Dictionary<string, int> options) : base(situation, options)
        { }

        public override void Add(Component c)
        {
            children.Add(c);
        }

        public override void Remove(Component c)
        {
            children.Remove(c);
        }
    }

    public class Leaf : Component
    {
        public Leaf(string situation, Dictionary<string, int> options) : base(situation, options) { }

        public override void Add(Component component)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Component component)
        {
            throw new NotImplementedException();
        }
    }
}
