using TurnBasedFantasy;

namespace TurnBasedFantasy
{
    public class UnitTest1
    {
        [Fact]
        public void TestDictionary()
        {
            var dict = PlayerTest.Perks.ToDictionary(
                perk => perk.AbilityId,
                perk => perk
                );

            Assert.IsType<Dictionary<int, Perk>>(dict);
        }

        [Fact]
        public void TestSortedList()
        {
            var sorted = PlayerTest.Perks.ToSortedList(perk => perk.Name).First().Value;
            var expected = "Archer";

            Assert.Equal(sorted.Name, expected);
        }

        [Fact]
        public void TestQueue()
        {
            var queuePerks = new Queue<Perk>(PlayerTest.Perks);
            var expected = 9;

            Assert.Equal(queuePerks.Peek().Level, expected);
        }

        [Fact]
        public void TestStack() 
        {
            var stackPerks = new Stack<Perk>(PlayerTest.Perks);

            Assert.Equal(stackPerks.Count(), 5);
            stackPerks.Pop();
            Assert.Equal(stackPerks.Count(), 4);
            stackPerks.Pop();
            Assert.Equal(stackPerks.Count(), 3);
        }

        [Fact]
        public void TestHashSet()
        {
            var perks = new HashSet<Perk>(PlayerTest.Perks);
            Assert.Equal(perks.Count(), PlayerTest.Perks.Count());
        }

        [Fact]
        public void TestGroup()
        {
            var listNames = PlayerTest.Abilities.GroupBy(p => p.Name);
            string[] expectedNames = { "Powershot", "Fireball", "Void", "Cloning", "Slaughtery" };

            Assert.Equal(expectedNames.Length, listNames.Count());

            foreach(var en in expectedNames)
            {
                Assert.Contains(en, listNames.Select(g => g.Key));
            }
        }

        [Fact]
        public void TestOrder()
        {
            var listNames = PlayerTest.Abilities.OrderBy(p => p.Name);
            string[] expectedNames = { "Cloning", "Fireball", "Powershot", "Slaughtery", "Void" };

            Assert.Equal(expectedNames.Length, listNames.Count());

            foreach (var en in expectedNames)
            {
                Assert.Contains(en, listNames.Select(g => g.Name));
            }
        }

        [Fact]
        public void TestDescOrder()
        {
            var listNames = PlayerTest.Abilities.OrderByDescending(p => p.Name);
            string[] expectedNames = { "Void", "Slaughtery", "Powershot", "Fireball", "Cloning" };

            Assert.Equal(expectedNames.Length, listNames.Count());

            foreach (var en in expectedNames)
            {
                Assert.Contains(en, listNames.Select(g => g.Name));
            }
        }


        [Fact]
        public void GroupDestructive()
        {
            var grouped = PlayerTest.Perks.Join(PlayerTest.Abilities,
                perk => perk.AbilityId,
                ability => ability.Id,
                (perk, ability) => new { P = perk, A = ability }).Where(isDest => isDest.A.IsDestructive == true).GroupBy(isDest => isDest.A.Id, isDest => isDest.P.Name)
                .SelectMany(m => m.ToList()).ToList();

            string[] expected = { "Fire Wizard", "Dark Wizard", "Archer" };

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.Equal(expected[i], grouped[i]);
            }
        }

        [Fact]
        public void ListAndDictionary()
        {
            List<Ability> a = new List<Ability>(PlayerTest.Abilities);
            Dictionary<int, Ability> d = PlayerTest.Abilities.ToDictionary(i => i.Id, a => a);
            
            Assert.Equal(a.Count, d.Count);

            a.Clear();
            d.Clear();

            Assert.Equal(a.Count-d.Count, 0); 
        }

        [Fact]
        public void AnonymClassAndInit()
        {
            // anonymous class
            var location = new
            {
                Name = "Dark Castle",
                DifficultyLevel = 5,
                HasDeadlyChoises = true
            };

            // creating an object using initializer
            Perk perk = new Perk { Name = "Nightsight", AbilityId = 53, Level = 6 };

            Assert.True(perk.Level >= location.DifficultyLevel);
        }

        [Fact]
        public void SortingWithComparer()
        {
            var abilities = PlayerTest.Abilities;
            string[] expected = { "Cloning", "Fireball", "Powershot", "Slaughtery", "Void" };

            AbilityComparer abilityComparer = new AbilityComparer();

            abilities.Sort(abilityComparer);

            for(int i = 0; i < abilities.Count; i++)
            {
                Assert.Equal(expected[i], abilities[i].Name);
            }

        }

        [Fact]
        public void ToArrayConvertation()
        {
            var perks = PlayerTest.Perks;
            var array = perks.ToArray();

            Assert.IsType<Perk[]>(array);
        }

        [Fact]
        public void TestArraySorting()
        {
            var abilities = PlayerTest.Abilities;
            var array = abilities.ToArray().OrderBy(n => n.Name).ToArray();

            string[] expectedNames = { "Cloning", "Fireball", "Powershot", "Slaughtery", "Void" };

            Assert.Equal(expectedNames.Length, array.Count());

            for(int i = 0; i < array.Length; i++)
            {
                Assert.Equal(array[i].Name, expectedNames[i]);
            }
        }

        [Fact]
        public void TestFirstNoExcep()
        {

        }
    }
}