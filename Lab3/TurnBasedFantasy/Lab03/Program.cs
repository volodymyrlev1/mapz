﻿using System.Text;
using TurnBasedFantasy;

namespace Lab03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //StringBuilder str = new StringBuilder();
            //str.Append(String.Format("{0, -15} {1, -10} {2, -15} {3, -15}\n", "Name", "Level", "Spell Name", "Is Destructive"));

            //foreach (var p in PlayerTest.Perks)
            //{
            //    var ability = PlayerTest.Abilities.Find(a => a.Id == p.AbilityId);
            //    str.Append(String.Format("{0, -15} {1, -10} {2, -15} {3, -15}\n", p.Name, p.Level,
            //        ability.Name.ToString(),
            //        ability.IsDestructive.ToString()
            //        ));
            //}

            //Console.WriteLine(str);

            // first 
            List<int> i = new List<int>() { 1, 2, 3 };

            Console.WriteLine(i.MFirst());

            //max and maxBy
            List<Student> students = new List<Student>
            {
                new Student { Name = "Alice", Score = 85 },
                new Student { Name = "Bob", Score = 90 },
                new Student { Name = "Charlie", Score = 78 }
            };

            Console.WriteLine(students.Max(a=>a.Score));
            Console.WriteLine(students.MaxBy(a=>a.Score).Name);
        }
    }

    class Student
    {
        public string Name { get; set; }
        public int Score { get; set; }
    }
}