﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedFantasy
{
    public static class PlayerExtensions
    {
        public static Dictionary<TKey, TValue> ToDictionary<TSource, TKey, TValue>(
                this IEnumerable<TSource> source,
                Func<TSource, TKey> keySelector,
                Func<TSource, TValue> valueSelector)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (keySelector == null)
                throw new ArgumentNullException(nameof(keySelector));
            if (valueSelector == null)
                throw new ArgumentNullException(nameof(valueSelector));

            var dictionary = new Dictionary<TKey, TValue>();
            foreach (var element in source)
            {
                dictionary[keySelector(element)] = valueSelector(element);
            }
            return dictionary;
        }
        // first noi exc,max maxBy

        public static T MFirst<T>(this IEnumerable<T> source)
        {
            if (source == null) 
                return default(T);

            if (!source.Any())
                return default(T);

            return source.First();
        }



        public static SortedList<TKey, TValue> ToSortedList<TValue, TKey>(
            this IEnumerable<TValue> src,
            Func<TValue, TKey> keySelect)
        {
            var sortedList = new SortedList<TKey, TValue>();
            foreach (var item in src)
            {
                sortedList.Add(keySelect(item), item);
            }
            return sortedList;
        }
    }

    public static class PlayerTest
    {
        public static string Name { get; set; }
        public static int Id { get; set; }

        public static List<Perk> Perks
        {
            get
            {
                return new List<Perk>()
                {
                    new Perk { Name = "Fire Wizard", Level = 9,AbilityId = 0},
                    new Perk { Name = "Sword Master", Level = 2, AbilityId = 1 },
                    new Perk { Name = "Dark Wizard", Level = 15, AbilityId = 2 },
                    new Perk { Name = "Archer", Level = 11, AbilityId = 3 },
                    new Perk { Name = "Cloner", Level = 17, AbilityId = 4 }
                };
            }
        }
        public static List<Ability> Abilities
        {
            get
            {
                return new List<Ability>()
                {
                    new Ability { Name = "Powershot", IsDestructive = true, Id = 3 },
                    new Ability { Name = "Fireball", IsDestructive = true, Id = 0 },
                    new Ability { Name = "Void", IsDestructive = true, Id = 2 },
                    new Ability { Name = "Cloning", IsDestructive = false, Id = 4 },
                    new Ability { Name = "Slaughtery", IsDestructive = false, Id = 1}
                };
            }
        }
    }

    public class Perk
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int AbilityId { get; set; }

        public Perk()
        {

        }

        public Perk(string name, int level, int abilityId)
        {
            Name = name;
            Level = level;
            AbilityId = abilityId;
        }
    }

    public class Ability
    {
        public string Name { get; set; }
        public bool IsDestructive { get; set; }
        public int Id { get; set; }
    }

    public class AbilityComparer : IComparer<Ability>
    {
        public int Compare(Ability? x, Ability? y)
        {
            if (x == null) return -1;
            else if( y == null) return 1;
            else if( x == null && y == null) return 0;

            int nameCmp = string.Compare(x.Name, y.Name);
            if( nameCmp != 0) return nameCmp;

            if (x.IsDestructive != y.IsDestructive)
            {
                return x.IsDestructive ? 1 : -1;
            }

            return x.Id.CompareTo(y.Id);
        }
    }
}
