﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogueFantasy
{
    public static class Dialogues
    {
        public static List<string> JoeDialogues = new List<string>
        {
            "Hey. Im Joe.",
            "I have a task for you.",
            "I need you to go to the cave."
        };

        public static List<string> BobDialogues = new List<string>
        {
            "Inch by inch, inch by inch",
            "The siege tower crawls across the battlefied."
        };

        public static List<string> CaveDialogues = new List<string>
        {
            "Minecraft cave type noises..."
        };
    }

    public static class GameDialogues
    {
        public static Dictionary<string, Dictionary<string,int>> PeacefulEvents = new Dictionary<string, Dictionary<string, int>>
        {
            { "You met a dog.", new Dictionary<string,int> { { "Pet it.", 1 }, { "Kick it.", 0 }, { "Give a bone.", 1 }, { "Step over the dog.", 1 } } },
            { "You found a friendly squirrel.", new Dictionary<string,int> { {"Feed it some nuts.", 1}, { "Take a photo.", 1 }, { "Chase it away gently.", 0 }, { "Watch it play.", 1 } } },
            { "You encounter a singing bird.", new Dictionary<string,int> { { "Listen to its song.", 1 }, { "Try to imitate.", 0 }, { "Offer it some seeds.", 1 }, { "Leave it in peace.", 1 } } },
            { "A rabbit hops across your path.", new Dictionary<string,int> { { "Admire its cuteness.", 1 }, { "Offer it a carrot.", 1 }, { "Try to catch it.", 0 }, { "Let it go on its way.", 1 } } },
            { "You stumble upon a peaceful stream.", new Dictionary<string,int> { { "Skip stones.", 1 }, { "Dip your feet.", 0 }, { "Collect colorful pebbles.", 1 }, { "Sit and enjoy the view.", 1 } } },
            { "You see a butterfly resting on a flower.", new Dictionary<string,int> { { "Observe it quietly.", 1 }, { "Take a photo.", 1 }, { "Catch it gently.", 0 }, { "Plant more flowers nearby.", 1 } } },
            { "A friendly cat approaches you.", new Dictionary<string,int> { { "Scratch its chin.", 1 }, { "Offer it some fish.", 1 }, { "Ignore it.", 0 }, { "Invite it to follow you.", 1 } } },
            { "You find a turtle sunbathing.", new Dictionary<string,int> { { "Watch it from a distance.", 1 }, { "Offer it some lettuce.", 1 }, { "Take a photo.", 0 }, { "Build a shade for it.", 1 } } },
            { "You come across a field of wildflowers.", new Dictionary<string,int> { { "Pick some for a bouquet.", 1 }, { "Lie down and relax.", 1 }, { "Sketch the scenery.", 0 }, { "Take a deep breath and enjoy the scent.", 1 } } },
            { "You spot a family of ducks swimming in a pond.", new Dictionary<string,int> { { "Feed them some breadcrumbs.", 1 }, { "Watch their antics.", 1 }, { "Try to count them.", 0 }, { "Take a quiet walk around the pond.", 1 } } },
            { "You find a friendly deer grazing.", new Dictionary<string,int> { { "Watch it quietly.", 1 }, { "Offer it some apples.", 1 }, { "Take a photo.", 0 }, { "Leave some food nearby.", 1 } } },
            { "A group of fireflies dances in the night.", new Dictionary<string,int> { { "Sit and watch their lights.", 1 }, { "Try to catch one.", 1 }, { "Take a photo.", 0 }, { "Sing softly along with their rhythm.", 1 } } },
            { "You discover a hidden garden.", new Dictionary<string,int> { { "Explore its paths.", 1 }, { "Smell the flowers.", 1 }, { "Sit on a bench and rest.", 0 }, { "Take a moment to meditate.", 1 } } },
            { "A small waterfall cascades down a rocky slope.", new Dictionary<string,int> { { "Listen to its soothing sound.", 1 }, { "Splash your face with its water.", 1 }, { "Take a photo.", 0 }, { "Build a small cairn nearby.", 1 } } },
            { "You encounter a friendly fox.", new Dictionary<string,int> { { "Observe it from a distance.", 1 }, { "Offer it some berries.", 1 }, { "Try to communicate with it.", 0 }, { "Leave a trail of breadcrumbs.", 1 } } },
            { "A group of otters playfully swim in a river.", new Dictionary<string,int> { { "Watch their antics.", 1 }, { "Try to imitate their swimming.", 1 }, { "Take a photo.", 0 }, { "Throw a small pebble for them to chase.", 1 } } },
            { "You discover a hidden cave filled with glowing crystals.", new Dictionary<string,int> { { "Admire their beauty.", 1 }, { "Collect some as souvenirs.", 1 }, { "Take a photo.", 0 }, { "Leave a small offering of gratitude.", 1 } } },
            { "You spot a rainbow after the rain.", new Dictionary<string,int> { { "Follow it to its end.", 1 }, { "Take a photo.", 1 }, { "Try to touch it.", 0 }, { "Sit and enjoy its colors.", 1 } } },
            { "A gentle breeze rustles through the trees.", new Dictionary<string,int> { { "Close your eyes and feel its caress.", 1 }, { "Listen to the leaves whisper.", 1 }, { "Take a deep breath.", 0 }, { "Dance with the wind.", 1 } } },
            { "You find a cozy spot under a big tree.", new Dictionary<string,int> { { "Read a book.", 1 }, { "Take a nap.", 1 }, { "Write in your journal.", 0 }, { "Listen to the sounds of nature.", 1 } } }
        };

        public static Dictionary<string, Dictionary<string, int>> DangerousEvents = new Dictionary<string, Dictionary<string, int>>
        {
            { "You stumble upon a cursed artifact!", new Dictionary<string, int> { { "Touch it to investigate.", 0 }, { "Leave it untouched and walk away.", 1 }, { "Attempt to destroy it.", 0 }, { "Seek out someone knowledgeable about curses.", 1 } } },
            { "A mysterious fog engulfs the area!", new Dictionary<string, int> { { "Try to navigate through it.", 0 }, { "Wait for it to dissipate.", 1 }, { "Look for a source of light.", 1 }, { "Use magic to dispel it.", 0 } } },
            { "You fall into a hidden pit of quicksand!", new Dictionary<string, int> { { "Struggle to escape.", 1 }, { "Keep calm and wait for help.", 1 }, { "Search for solid ground.", 0 }, { "Use magic or tools to create a stable surface.", 1 } } },
            { "A dark presence lurks in the shadows!", new Dictionary<string, int> { { "Confront it head-on.", 0 }, { "Attempt to communicate with it.", 1 }, { "Flee from the area.", 1 }, { "Use protective spells or items.", 1 } } },
            { "You trigger a trap set by ancient guardians!", new Dictionary<string, int> { { "Try to disarm it.", 0 }, { "Dodge the incoming danger.", 1 }, { "Activate countermeasures.", 0 }, { "Seek knowledge on how to bypass such traps.", 1 } } },
            { "You disturb an ancient burial ground!", new Dictionary<string, int> { { "Investigate further.", 1 }, { "Apologize and leave offerings.", 0 }, { "Attempt to communicate with restless spirits.", 1 }, { "Perform a ritual to cleanse the area.", 0 } } },
            { "A powerful storm suddenly approaches!", new Dictionary<string, int> { { "Take shelter.", 1 }, { "Brace against the elements.", 1 }, { "Use magic to calm the storm.", 0 }, { "Search for a way to control the weather.", 1 } } },
            { "You encounter a group of hostile mercenaries!", new Dictionary<string, int> { { "Negotiate for safe passage.", 1 }, { "Prepare for combat.", 0 }, { "Attempt to sneak past them.", 1 }, { "Offer them a valuable item in exchange for passage.", 0 } } },
            { "A cursed forest twists and distorts reality!", new Dictionary<string, int> { { "Navigate cautiously.", 1 }, { "Seek out a guide familiar with the forest.", 1 }, { "Use magic to create a path.", 0 }, { "Find a way to dispel the curse.", 1 } } },
            { "You disturb the lair of a powerful dragon!", new Dictionary<string, int> { { "Offer tribute in exchange for safe passage.", 0 }, { "Try to reason with the dragon.", 1 }, { "Flee from the area.", 1 }, { "Prepare for a battle.", 1 } } },
            { "You awaken an ancient golem guardian!", new Dictionary<string, int> { { "Attempt to deactivate it.", 0 }, { "Run for cover.", 1 }, { "Use magic to control it.", 0 }, { "Find its creator and seek their help.", 1 } } },
            { "You fall into a portal to a hostile dimension!", new Dictionary<string, int> { { "Explore the new dimension.", 0 }, { "Try to find a way back.", 1 }, { "Befriend the locals.", 1 }, { "Confront the ruler of the dimension.", 1 } } },
            { "You disturb the resting place of cursed spirits!", new Dictionary<string, int> { { "Offer prayers for forgiveness.", 1 }, { "Attempt to cleanse the area.", 0 }, { "Flee from the spirits.", 1 }, { "Seek out a powerful exorcist.", 0 } } },
            { "A swarm of enchanted creatures attacks!", new Dictionary<string, int> { { "Defend yourself with magic.", 1 }, { "Seek out their leader.", 1 }, { "Try to break their enchantment.", 0 }, { "Find a way to communicate peacefully.", 0 } } },
            { "You trigger a trap set by mischievous fairies!", new Dictionary<string, int> { { "Offer them a gift to appease them.", 0 }, { "Try to outwit them.", 1 }, { "Seek help from a fairy-friendly creature.", 0 }, { "Find a way to reverse the trap's effects.", 1 } } },
            { "You disturb an ancient elemental guardian!", new Dictionary<string, int> { { "Attempt to calm it.", 1 }, { "Flee from the area.", 1 }, { "Offer it an offering of elemental essence.", 0 }, { "Use magic to bind or control it.", 0 } } },
            { "A powerful curse befalls upon you!", new Dictionary<string, int> { { "Seek out a powerful sorcerer to lift the curse.", 0 }, { "Search for a rare antidote.", 1 }, { "Embrace the curse and seek its benefits.", 0 }, { "Find a way to transfer the curse to another.", 1 } } },
            { "You fall into a labyrinth of illusions!", new Dictionary<string, int> { { "Trust your instincts to navigate.", 1 }, { "Search for clues to the true path.", 0 }, { "Use magic to dispel illusions.", 0 }, { "Find a way to trick the labyrinth.", 1 } } },
            { "A dark wizard casts a spell of confusion upon you!", new Dictionary<string, int> { { "Focus your mind to break free.", 1 }, { "Seek out a counter-spell.", 0 }, { "Try to reason with the wizard.", 0 }, { "Embrace the confusion and let it guide you.", 1 } } },
            { "You disturb the resting place of an ancient vampire!", new Dictionary<string, int> { { "Offer it a deal for safe passage.", 0 }, { "Flee from the area.", 0   }, { "Find its coffin and destroy it.", 1 }, { "Confront the vampire and negotiate for freedom.", 1 } } }
        };


        public static List<string> PositiveBonuses = new List<string>
        {
            "You gained agility.",
            "You became invisible for a short period of time.",
            "Now you can resurrect yourself one time.",
            "You found a potion that increases your strength.",
            "A magical cloak grants you protection from harm.",
            "You learned a new powerful spell.",
            "You befriended a mythical creature that aids you in battles.",
            "A blessing from a deity grants you divine protection.",
            "You discovered a hidden artifact that enhances your abilities.",
            "You gained the ability to teleport short distances.",
            "You unlocked a hidden talent within yourself.",
            "You received a boon that increases your luck in finding treasure.",
            "A guardian spirit watches over you, granting you protection.",
            "You found a pair of enchanted boots that allow you to walk on water.",
            "A fairy blessed you with the gift of healing.",
            "You gained the ability to speak with animals.",
            "You acquired a magical amulet that grants you immortality.",
            "You unlocked the power of elemental manipulation.",
            "You discovered a spell that grants you temporary invincibility.",
            "You found a magical mirror that reveals hidden truths."
        };

        public static List<string> NegativeBonuses = new List<string>()
        {
            "You got poisoned.",
            "You are now cursed with bad luck.",
            "You lost a limb in battle.",
            "A dark aura surrounds you, attracting hostile creatures.",
            "You are now haunted by vengeful spirits.",
            "You became half-blind due to a magical mishap.",
            "Your strength has been drained by a malevolent force.",
            "You are now vulnerable to magic attacks.",
            "You are afflicted with a debilitating disease.",
            "You are now hunted by a powerful enemy.",
            "You've lost your memory due to a magical accident.",
            "You are now unable to speak due to a curse.",
            "Your mind has been invaded by dark thoughts.",
            "You've been cursed to never find restful sleep.",
            "You've been marked by a powerful demon.",
            "You are now susceptible to illusions and mind control.",
            "Your senses have been dulled by a magical trap.",
            "You've been cursed with eternal hunger.",
            "Your lifespan has been shortened by dark magic.",
            "You are now burdened with a heavy curse."
        };
    }
}
