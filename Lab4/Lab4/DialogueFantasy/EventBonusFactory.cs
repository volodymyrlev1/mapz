﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DialogueFantasy
{
    public interface IEvent
    {
        Tuple<string, Dictionary<string,int>> GetEvent();
    }

    public interface IBonus
    {
        string GetBonus();
    }

    public class PeacefulEvent : IEvent
    {
        private static readonly Random random = new Random();
        public Tuple<string, Dictionary<string, int>> GetEvent()
        {
            var index = random.Next(GameDialogues.PeacefulEvents.Count);
            var peacefulEvent = GameDialogues.PeacefulEvents.ElementAt(index);
            //GameDialogues.PeacefulEvents.Remove(peacefulEvent.Key);
            return new Tuple<string, Dictionary<string, int>>(peacefulEvent.Key, peacefulEvent.Value);
        }
    }

    public class DangerousEvent : IEvent
    {
        private static readonly Random random = new Random();
        public Tuple<string, Dictionary<string, int>> GetEvent()
        {
            var index = random.Next(GameDialogues.DangerousEvents.Count);
            var dangerousEvent = GameDialogues.DangerousEvents.ElementAt(index);
            //GameDialogues.DangerousEvents.Remove(dangerousEvent.Key);
            return new Tuple<string, Dictionary<string, int>>(dangerousEvent.Key, dangerousEvent.Value);
        }
    }

    public class PositiveBonus : IBonus
    {
        private static readonly Random random = new Random();
        public string GetBonus()
        {
            var index = random.Next(GameDialogues.PositiveBonuses.Count);
            var bonus = GameDialogues.PositiveBonuses[index];
            //GameDialogues.PositiveBonuses.RemoveAt(index);
            return bonus;
        }
    }

    public class NegativeBonus : IBonus
    {
        private static readonly Random random = new Random();
        public string GetBonus()
        {
            var index = random.Next(GameDialogues.NegativeBonuses.Count);
            var bonus = GameDialogues.NegativeBonuses[index];
            //GameDialogues.NegativeBonuses.RemoveAt(index);
            return bonus;
        }
    }

    public interface IEventBonusFactory
    {
        IEvent CreateEvent();
        IBonus CreateBonus();
    }

    public class PeacefulEventPositiveBonusFactory : IEventBonusFactory
    {
        public IEvent CreateEvent()
        {
            return new PeacefulEvent();
        }
        public IBonus CreateBonus()
        {
            return new PositiveBonus();
        }
    }

    public class DangerousEventNegativeBonusFactory : IEventBonusFactory
    {
        public IEvent CreateEvent()
        {
            return new DangerousEvent();
        }
        public IBonus CreateBonus()
        {
            return new NegativeBonus();
        }
    }
}
