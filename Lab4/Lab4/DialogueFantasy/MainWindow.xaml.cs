﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DialogueFantasy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Player player;
        public PlayerController playerController;
        public GameManager gm;
        public Character joe;
        public Character bob;

        public TextBlock eventTextBlock;
        public TextBlock bonusTextBlock;
        public Button[] choiceButtons;

        //props
        public Character cave;
        StackPanel questPanel;

        public static uint Level = 1;


        public MainWindow()
        {
            InitializeComponent();
            player = Player.GetInstance();
            playerController = new PlayerController(player, GameArea);
            gm = GameManager.Instance;
            gm.NewLevel += OnNewLevel;
            this.KeyDown += Window_KeyDown;
            this.KeyUp += Window_KeyUp;
   

            // joe
            joe = new Character("Joe", "res/joe.png", 1600, 700);
            joe.AddToCanvas(GameArea);
            joe.dialogues = Dialogues.JoeDialogues;

            // bob
            bob = (Character)joe.Clone();
            bob.name = "Bob";
            bob.characterImage.Source = new BitmapImage(new Uri("res/bob.png", UriKind.Relative));
            bob.SetPosition(70, 650);
            bob.AddToCanvas(GameArea);
            bob.dialogues = Dialogues.BobDialogues;

            //cave
            cave = (Character)joe.Clone();
            cave.name = "cave";
            cave.characterImage.Source = new BitmapImage(new Uri("res/cave.png", UriKind.Relative));
            cave.characterImage.Width = 300;
            cave.characterImage.Height = 300;
            cave.SetPosition(1050, 50);
            cave.AddToCanvas(GameArea);
            cave.Level = 1;
            cave.dialogues = Dialogues.CaveDialogues;

            player.HealthChanged += OnHealthChanged;
            gm.LevelsFinished += OnLevelsFinished;
            player.Health = 3;
        }

        private HashSet<Key> keysPressed = new HashSet<Key>();
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            keysPressed.Add(e.Key);
            playerController.Move(keysPressed.ToArray(), GameArea.ActualWidth, GameArea.ActualHeight);
            CheckPlayerNearby(e);
        }
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            keysPressed.Remove(e.Key);
            Player.GetInstance().playerImage.Source = new BitmapImage(new Uri("res/character.png", UriKind.Relative));
        }

        private void CheckPlayerNearby(KeyEventArgs e)
        {
            CheckPlayerNearCharacter(joe,e);
            CheckPlayerNearCharacter(bob,e);
            CheckPlayerNearQuest(cave, e);
        }

        private void CheckPlayerNearCharacter(Character c,KeyEventArgs e)
        {
            double playerX = Canvas.GetLeft(player.playerImage);
            double playerY = Canvas.GetTop(player.playerImage);
            double cX = c.xPosition;
            double cY = c.yPosition;

            double proximityThreshold = 100;

            if (Math.Abs(playerX - cX) < proximityThreshold && Math.Abs(playerY - cY) < proximityThreshold && e.Key == Key.E)
            {
                c.Talk();
            }
            else
            {
                c.dialogueTextBlock.Text = "";
            }
        }

        private void CheckPlayerNearQuest(Character q, KeyEventArgs e)
        {
            double playerX = Canvas.GetLeft(player.playerImage);
            double playerY = Canvas.GetTop(player.playerImage);
            double qX = q.xPosition;
            double qY = q.yPosition;

            double proximityThreshold = 100;

            if (Math.Abs(playerX - qX) < proximityThreshold && Math.Abs(playerY - qY) < proximityThreshold && e.Key == Key.E)
            {
                q.Talk();
                InitializeUI();
                gm.StartGame(GameArea);
            }
            else
            {
                q.dialogueTextBlock.Text = "";
            }
        }

        private void InitializeUI()
        {
            eventTextBlock = new TextBlock();
            bonusTextBlock = new TextBlock();
            choiceButtons = new Button[4];

            questPanel = new StackPanel();
            questPanel.Width = 500;
            questPanel.Height = 300;
            questPanel.Background = Brushes.White;

            questPanel.Loaded += (sender, e) =>
            {
                double left = (GameArea.ActualWidth - questPanel.ActualWidth) / 2;
                double top = (GameArea.ActualHeight - questPanel.ActualHeight) / 2;
                Canvas.SetLeft(questPanel, left);
                Canvas.SetTop(questPanel, top);
            };

            bonusTextBlock.Foreground = Brushes.Magenta;

            bonusTextBlock.HorizontalAlignment = HorizontalAlignment.Center;
            eventTextBlock.HorizontalAlignment = HorizontalAlignment.Center;

            questPanel.Children.Add(bonusTextBlock);
            questPanel.Children.Add(eventTextBlock);

            for (int i = 0; i < 4; i++)
            {
                choiceButtons[i] = new Button();
                choiceButtons[i].Content = "Button " + (i + 1);
                choiceButtons[i].Margin = new Thickness(0, 10, 0, 10);
                choiceButtons[i].Click += ChoiceButton_Click;
                choiceButtons[i].Width = 0.6 * questPanel.Width;
                questPanel.Children.Add(choiceButtons[i]);
            }

            GameArea.Children.Add(questPanel);
        }

        private void OnNewLevel(object sender, NewLevelEventArgs e)
        {
            DisplayEvent(e.Event);
            DisplayChoices(e.Choices);
            DisplayBonus(e.Bonus);

            //Random random = new Random();
            //int randomNumber = random.Next(1, 21);
            //Image backgroundImage = new Image();

            //string path = $"res/{randomNumber}.jpg";

            //backgroundImage.Source = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute));
            //backgroundImage.Stretch = Stretch.Fill;

            //GameArea.Background = new ImageBrush()
            //{
            //    ImageSource = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute)),
            //    Stretch = Stretch.Fill
            //};
        }

        private void DisplayEvent(string eventText)
        {
            eventTextBlock.Text = eventText;
        }

        private void DisplayChoices(Dictionary<string,int> choices)
        {
            for (int i = 0; i < 4; i++)
            {
                if (i < choices.Count)
                {
                    choiceButtons[i].Content = choices.Keys.ElementAt(i);
                    choiceButtons[i].IsEnabled = true;
                }
                else
                {
                    choiceButtons[i].IsEnabled = false;
                }
            }
        }

        private void DisplayBonus(string bonus)
        {
            bonusTextBlock.Text = bonus;
        }

        private void ChoiceButton_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = (Button)sender;
            int choiceIndex = Array.IndexOf(choiceButtons, clickedButton);
            gm.HandleChoice(eventTextBlock.Text, clickedButton.Content.ToString());
        }

        private void OnHealthChanged(object sender, EventArgs e)
        {
            UpdateHealth();
        }
        private void UpdateHealth()
        {
            HealthBar.Children.Clear();

            for (int i = 0; i < player.Health; i++)
            {
                Image heart = new Image();
                heart.Source = new BitmapImage(new Uri(@"pack://application:,,,/DialogueFantasy;component/res/heart.png", UriKind.Absolute));
                heart.Width = 100;
                heart.Height = 100;
                HealthBar.Children.Add(heart);
            }
        }

        private void OnLevelsFinished(object sender, EventArgs e)
        {
            CloseQuest();
        }
        private void CloseQuest()
        {
            questPanel.Visibility = Visibility.Collapsed;
        }
    }
}
