﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DialogueFantasy
{
    // Prototype pattern
    interface IClone
    {
        object Clone();
    }

    public class Character : IClone
    {
        public Image characterImage { get; set; }
        public double xPosition { get; set; }
        public double yPosition { get; set; }
        public string name { get; set; }
        public int? Level { get; set; }

        public List<string> dialogues = new List<string>();
        public TextBlock dialogueTextBlock { get; set; }


        public Character(string characterName, string imagePath, double initialX, double initialY)
        {
            name = characterName;
            characterImage = new Image();
            characterImage.Source = new BitmapImage(new Uri(imagePath, UriKind.Relative));
            characterImage.Width = 200;
            characterImage.Height = 200;
            xPosition = initialX;
            yPosition = initialY;

            dialogueTextBlock = new TextBlock
            {
                FontSize = 24,
                Foreground = Brushes.Black,
                TextWrapping = TextWrapping.Wrap,
                Text = ""
            };

            SetPosition(initialX, initialY);
            Level = null;
        }

        public void SetPosition(double newX, double newY)
        {
            xPosition = newX;
            yPosition = newY;
            Canvas.SetLeft(characterImage, xPosition);
            Canvas.SetTop(characterImage, yPosition);
            Canvas.SetLeft(dialogueTextBlock, xPosition);
            Canvas.SetTop(dialogueTextBlock, yPosition + characterImage.Height);
        }

        public void AddToCanvas(Canvas canvas)
        {
            canvas.Children.Add(characterImage);
            canvas.Children.Add(dialogueTextBlock);
        }

        public async void Talk()
        {
            foreach(var r in dialogues)
            {
                this.dialogueTextBlock.Text = r;
                await Task.Delay(2000);
            }
        }

        public object Clone()
        {
            return new Character(name, characterImage.Source.ToString(), xPosition, yPosition)
            {
                dialogues = new List<string>(dialogues),
                Level = Level
            };
        }
    }
}
