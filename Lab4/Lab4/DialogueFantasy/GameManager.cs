﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace DialogueFantasy
{
    public sealed class GameManager
    {
        public int GameCounter { get; set; } = 1;
        public StackPanel stackPanel;

        private static GameManager instance;
        private static readonly object lockObj = new object();

        private static readonly Random random = new Random();
        private IEventBonusFactory eventBonusFactory;

        private int _levelCounter;
        private int LevelCounter
        {
            get => _levelCounter;
            set
            {
                if(_levelCounter == 10)
                {
                    _levelCounter = 1;

                    OnLevelsFinished(EventArgs.Empty);
                }

                _levelCounter++;
            }
        }
        


        private IEvent currentEvent;

        public event EventHandler<NewLevelEventArgs> NewLevel;
        public event EventHandler LevelsFinished;

        public void OnLevelsFinished(EventArgs e)
        {
            LevelsFinished?.Invoke(this, e);
        }


        private GameManager()
        {
            _levelCounter = 1;
            SetRandomFactory();
        }

        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObj)
                    {
                        if (instance == null)
                        {
                            instance = new GameManager();
                        }
                    }
                }
                return instance;
            }
        }

        private void SetRandomFactory()
        {
            int randomIndex = random.Next(2);
            eventBonusFactory = randomIndex == 0
                ? (IEventBonusFactory)new PeacefulEventPositiveBonusFactory()
                : new DangerousEventNegativeBonusFactory();
        }

        public void StartGame(Canvas canvas)
        {
            PlayLevel();
        }

        private void PlayLevel()
        {
            currentEvent = eventBonusFactory.CreateEvent();
            var (eventText, choices) = currentEvent.GetEvent();
            string bonus = string.Empty;

            if (LevelCounter % 3 == 0)
            {
                bonus = eventBonusFactory.CreateBonus().GetBonus();
            }

            OnNewLevel(new NewLevelEventArgs(eventText, choices, bonus));
            LevelCounter++;
            SetRandomFactory();
        }

        public void HandleChoice(int choiceIndex)
        {
            PlayLevel();
        }
        public void HandleChoice(string ev, string choice)
        {

            int value = -1;

            try
            {
                value = GameDialogues.PeacefulEvents[ev][choice];
            }
            catch(Exception e)
            {
                
               value = GameDialogues.DangerousEvents[ev][choice];
            }

            var p = Player.GetInstance();

            if (value == 0) p.Health -= 1;

            PlayLevel();
        }

        private void OnNewLevel(NewLevelEventArgs e)
        {
            NewLevel?.Invoke(this, e);
        }
    }

    public class NewLevelEventArgs : EventArgs
    {
        public string Event { get; }
        public Dictionary<string, int> Choices { get; }
        public string Bonus { get; }

        public NewLevelEventArgs(string eventText, Dictionary<string, int> choices, string bonus)
        {
            Event = eventText;
            Choices = choices;
            Bonus = bonus;
        }
    }
}
