﻿using System;
using System.Diagnostics;

namespace PartTwo
{
    class BoxingUnboxingTest
    {
        public void CompareIntOperations()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            int intValue = 10;
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = intValue;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for int: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = 10;
            for (int i = 0; i < iterations; i++)
            {
                intValue = (int)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for int: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            for (int i = 0; i < iterations; i++)
            {
                intValue = 10;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for int: {sw.ElapsedMilliseconds} ms");
        }

        public void CompareDoubleOperations()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            double doubleValue = 10.5;
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = doubleValue;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for double: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = 10.5;
            for (int i = 0; i < iterations; i++)
            {
                doubleValue = (double)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for double: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            for (int i = 0; i < iterations; i++)
            {
                doubleValue = 10.5;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for double: {sw.ElapsedMilliseconds} ms");
        }

        public void CompareFloatOperations()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            float floatValue = 10.5f;
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = floatValue;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for float: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = 10.5f;
            for (int i = 0; i < iterations; i++)
            {
                floatValue = (float)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for float: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            for (int i = 0; i < iterations; i++)
            {
                floatValue = 10.5f;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for float: {sw.ElapsedMilliseconds} ms");
        }

    }

    struct MyStruct
    {
        public int f1;
        public int f2;
        public int f3;
        public int f4;
        public int f5;
        public int f6;
        public int f7;
        public int f8;
        public int f9;
        public int f10;
    }

    class StructureComparison
    {
        public void CompareStructures()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            MyStruct str = new MyStruct();
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = str;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for struct: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = str;
            for (int i = 0; i < iterations; i++)
            {
                str = (MyStruct)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for struct: {sw.ElapsedMilliseconds} ms");

            sw.Start();
            MyStruct str2 = new MyStruct();
            for (int i = 0; i < iterations; i++)
            {
                str = str2;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for struct: {sw.ElapsedMilliseconds} ms");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            //BoxingUnboxingTest BU = new BoxingUnboxingTest();

            //BU.CompareFloatOperations();
            //Console.ReadLine();

            StructureComparison SC = new StructureComparison();

            SC.CompareStructures();
            Console.ReadLine();
        }
    }
}