﻿using System;
using System.Diagnostics;

namespace PartTwo
{
    class BoxingUnboxingTest
    {
        public void CompareIntOperations()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            int intValue = 10;
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = intValue;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for int: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = 10;
            for (int i = 0; i < iterations; i++)
            {
                intValue = (int)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for int: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            for (int i = 0; i < iterations; i++)
            {
                intValue = 10;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for int: {sw.ElapsedMilliseconds} ms");
        }

        public void CompareDoubleOperations()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            double doubleValue = 10.5;
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = doubleValue;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for double: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = 10.5;
            for (int i = 0; i < iterations; i++)
            {
                doubleValue = (double)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for double: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            for (int i = 0; i < iterations; i++)
            {
                doubleValue = 10.5;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for double: {sw.ElapsedMilliseconds} ms");
        }

        public void CompareFloatOperations()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            float floatValue = 10.5f;
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = floatValue;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for float: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = 10.5f;
            for (int i = 0; i < iterations; i++)
            {
                floatValue = (float)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for float: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            for (int i = 0; i < iterations; i++)
            {
                floatValue = 10.5f;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for float: {sw.ElapsedMilliseconds} ms");
        }

    }

    class MyClass
    {
        public int f1;
    }

    interface IStructCompare
    {
        public void CompareStructures();
    }

    struct StructureComparison : IStructCompare
    {
        public void CompareStructures()
        {
            const int iterations = 10000000;
            Stopwatch sw = new Stopwatch();

            MyClass cls = new MyClass();
            object obj;

            sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                obj = cls;
            }
            sw.Stop();
            Console.WriteLine($"Boxing for class: {sw.ElapsedMilliseconds} ms");

            sw.Restart();
            obj = cls;
            for (int i = 0; i < iterations; i++)
            {
                cls = (MyClass)obj;
            }
            sw.Stop();
            Console.WriteLine($"Unboxing for class: {sw.ElapsedMilliseconds} ms");

            sw.Start();
            MyClass str2 = new MyClass();
            for (int i = 0; i < iterations; i++)
            {
                cls = str2;
            }
            sw.Stop();
            Console.WriteLine($"Assignment for class: {sw.ElapsedMilliseconds} ms");
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            //BoxingUnboxingTest BU = new BoxingUnboxingTest();

            //BU.CompareFloatOperations();
            //Console.ReadLine();

            StructureComparison SC = new StructureComparison();

            SC.CompareStructures();
            Console.ReadLine();
        }
    }
}