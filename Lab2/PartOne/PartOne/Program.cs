﻿namespace PartOne;

interface IInteraction
{
    void Interact();
}

public abstract class NPC : IInteraction
{
    protected string Race { get; set; }
    protected internal double HealthPoints { get; set; }
    private protected double Damage { get; set; }

    public abstract void React();

    public void Interact()
    {
        Console.WriteLine("NPC interacts");
    }

    public void PerformExtraAction()
    {
        Console.WriteLine("NPC doing smthng");
    }
}

public class QuestGiver : NPC
{
    internal string Guild {  get; set; }

    public QuestGiver()
    {
        Race = string.Empty;
        HealthPoints = 100;
        Damage = 10;
    }

    public override void React()
    {
        Console.WriteLine("Quest giver reacts");
    }
}

public class Character
{
    public string Name { get; set; }
    private int ID { get; set; }

    public void PerformAction()
    {
        Console.WriteLine("Do smthng");
    }
}

struct Quest
{
    public string Name;         
    public string Description;  
    public int Reward;          
    public bool IsCompleted;    
    public int RequiredLevel;

    string EstimatedDifficulty;
}

class Town
{
    Town()
    {
        Console.WriteLine("Create Town");
    }
    uint Population { get; set; }

    string GetTownInfo()
    {
        Building building = new Building();

        building.Type = "Smith";

        return "";
    }


    class Building
    {
        public string Type { get; set; }
        private int Capacity { get; set; }
    }
}


internal class Program
{
    static void Main(string[] args)
    {
        int n = 1;
        object obj = n; // boxing

        Console.WriteLine($"{(int)obj}");

        int m = (int)obj; //unboxing
        Console.WriteLine(m);
    }
}
