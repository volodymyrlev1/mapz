﻿using PartOne;
using System.Globalization;

namespace PartOneA2;

public interface ICharacter
{
    void Interact();
}

public interface IItem
{
    void Use();
}

public class Entity: ICharacter, IItem
{
    public Entity() 
    {
    
    }

    void ICharacter.Interact()
    {

    }

    void IItem.Use() 
    {
    
    }
}




public class Character : Entity
{
    public string Name { get; set; }
    public static int StartingHealth { get; set; }

    static Character()
    {
        StartingHealth = 100;
        //Console.WriteLine("Character constructor STATIC");
    }

    public Character()
    {
        Name = "Unknown";
        //Console.WriteLine("Character constructor ()");
    }

    public Character(string n)
    {
        this.Name = n;
        //Console.WriteLine("Character constructor (string n)");
    }
    public Character GetCharacter()
    {
        return this;
    }

    public void WriteCharacterData(out string n)
    {
        n = this.Name;
    }
}

public class Hero : Character
{
    public int Level { get; set; }
    public static int MaxLevel { get; set; }

    static Hero()
    {
        MaxLevel = 120;
        //Console.WriteLine("Hero constructor STATIC");
    }

    public Hero() : base("Unknown")
    {
        this.Level = 0;
        //Console.WriteLine("Hero constructor ()");
    }

    public Hero(string n, int l) : base(n)
    {
        this.Level = l;
        //Console.WriteLine("Hero constructor (string n, int l)");
    }

    public static implicit operator int(Hero h)
    {
        return h.Level;
    }

    public static implicit operator double(Hero h)
    {
        return (double)h.Level;
    }


    public override string ToString()
    {
        return "Hero class";
    }
    public override bool Equals(object? obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (obj.GetType() != this.GetType())
        {
            return false;
        }

        Hero other = (Hero)obj;
        return this.Level == other.Level;
    }

    public override int GetHashCode()
    {
        return Level.GetHashCode();
    }

    protected virtual void Finalize()
    {

    }
}

internal class Program
{
    public static void ChangeCharacterName(Character c, string n)
    {
        c.Name = n;
    }

    static void Main(string[] args)
    {
        //Character Volodia = new Character("Volodia");

        //Console.WriteLine($"Before: {Volodia.Name}");
        //ChangeCharacterName(Volodia, "Mykhailo");
        //Console.WriteLine($"After: {Volodia.Name}");

        //// explicit
        //double a = 4.20;
        //int x = (int)a;
        ////Console.WriteLine(x);

        //// implicit
        //double y = x;
        ////Console.WriteLine(y);

        //Hero H = new Hero("Volodia", 18);
        //int b = H;
        //Console.WriteLine($"{b} {(int)H} {(double) H}");


        Hero Hero1 = new Hero("Hero1", 28);
        Hero Hero2 = new Hero("Hero2", 28);

        Console.WriteLine($"{Hero1.ToString()}");
        Console.WriteLine($"{Hero1.GetHashCode()} {Hero2.GetHashCode()}");
        Console.WriteLine($"{Hero1.Equals(Hero2)}");
    }
}