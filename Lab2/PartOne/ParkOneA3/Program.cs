﻿namespace ParkOneA3
{
    public enum SituationAction
    {
        GoRight,
        GoLeft,
        GoStraight,
        PetDog,
        KickDog,
        ThrowSausage
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            SituationAction action = SituationAction.GoRight;
            SituationAction action1 = SituationAction.PetDog;

            Console.WriteLine($"{action == SituationAction.GoRight}");
            Console.WriteLine($"{SituationAction.GoLeft == SituationAction.GoRight}");
            Console.WriteLine($"{SituationAction.GoLeft != SituationAction.GoRight}");
            Console.WriteLine($"{((int)action == 0) && (action == SituationAction.GoRight) && ((int)action1 == (int)SituationAction.PetDog)}");
            Console.WriteLine($"{((int)action == 420) || (action1 == SituationAction.PetDog)}");
            Console.WriteLine($"{!(action == SituationAction.GoRight)}");
            Console.WriteLine($"{(action^action1) == SituationAction.PetDog}");
            Console.WriteLine($"{~(int)action1}");

            SituationAction a1 = SituationAction.GoRight | SituationAction.PetDog;
            SituationAction a2 = SituationAction.GoRight | SituationAction.KickDog;

            Console.WriteLine($"{a1^a2}");
            Console.WriteLine($"{a1&a2}");
            Console.WriteLine($"{a1|a2}");


        }
    }
}